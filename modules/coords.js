const drawCoords = (ctx) => {

    ctx.beginPath();
    ctx.moveTo(0,-350);
    ctx.lineTo(0,350);
    ctx.strokeStyle = `rgba(0,0,0,0.4)`;
    ctx.lineWidth = 1;
    ctx.stroke();

    ctx.beginPath();
    ctx.moveTo(-350,0);
    ctx.lineTo(350,0);
    ctx.strokeStyle = `rgba(0,0,0,0.4)`;
    ctx.lineWidth = 1;
    ctx.stroke();

    for (let i = -350; i <= 350; i += 25) {
        ctx.beginPath();
        ctx.moveTo(i,-350);
        ctx.lineTo(i,350);
        ctx.strokeStyle = `rgba(0,0,0,0.1)`;
        ctx.lineWidth = 1;
        ctx.stroke();
    }
    for (let i = -350; i <= 350; i += 25) {
        ctx.beginPath();
        ctx.moveTo(-350,i);
        ctx.lineTo(350,i);
        ctx.strokeStyle = `rgba(0,0,0,0.1)`;
        ctx.lineWidth = 1;
        ctx.stroke();
    }
}
export default drawCoords;
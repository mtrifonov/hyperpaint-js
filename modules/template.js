const templateLib = {
};
const template_ = (state,{id}) => {
    const keys = Object.keys(templateLib);
    let templateFound = false;
    if (!id) {
        console.error("Template ID missing");
        return;
    };
    for (let i = 0; i < keys.length; i++) {
        if(keys[i]===id) {
            templateLib[keys[i]](state);
            templateFound = true;
            break;
        };
    }
    if (!templateFound) {
        console.error(`Couldn't find template: ${id}`);
        return;
    };

};
export default template_;
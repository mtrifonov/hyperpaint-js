const errors = {
forward: `Error at "forward()". No argument or wrong argument type.
The argument passed to "forward()" must be a number.`,
backward: `Error at "backward()". No argument or wrong argument type.
The argument passed to "forward()" must be a number.`,
goTo: `Error at "goTo()". Not enough arguments, or wrong argument type.
The arguments passed to "goto()" must be two numbers.`,
right: `Error at "right()". No argument or wrong argument type.
The argument passed to "right()" must be a number.`, 
left: `Error at "left()". No argument or wrong argument type.
The argument passed to "left()" must be a number.`, 
lineForward: `Error at "lineForward()". No argument or wrong argument type.
The argument passed to "lineForward()" must be a number.`,
lineTo: `Error at "lineTo()". Not enough arguments, or wrong argument type.
The arguments passed to "lineto()" must be two numbers.`,
drawCircle: `Error at "drawCircle()". No argument or wrong argument type.
The argument passed to "drawCircle()" must be a number.`,
setColor: `Error at "setColor()". No argument(s) or wrong argument type(s).
Did not recognize color.`,
setWidth: `Error at "setWidth()". No argument or wrong argument type.
The argument passed to "setWidth()" must be a number.`,
};

export default errors;

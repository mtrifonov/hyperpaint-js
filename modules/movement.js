import errorMessages from './error_messages.js';

const forward = (state,{dist}) => {
    let {arrow: {angle, position: {x,y}}} = state;
    if (typeof dist === 'number') {
        let y_delta = Math.cos(angle) * dist;
        let x_delta = Math.sin(angle) * dist;
        x = x + x_delta;
        y = y + y_delta;
        goTo(state,{x,y});
    } else state.errors.push(errorMessages.forward);

}

const backward = (state,{dist}) => {
    if (typeof dist === 'number') {
        forward(state,{dist:-dist});
    } else state.errors.push(errorMessages.backward);
}

const goTo = (state,{x,y}) => {
    let {arrow,vctx} = state
    if (typeof x === 'number' && typeof y === 'number') {
        arrow.position.x = x;
        arrow.position.y = y;
    } else state.errors.push(errorMessages.goTo);

}

const right = (state,{deg}) => {
    let {arrow,vctx} = state;
    if (typeof deg === 'number') {
        deg %= 360;
        let rad = Math.PI * 2 * (deg / 360);
        arrow.angle += rad;
    } else state.errors.push(errorMessages.right);

}

const left = (state,{deg}) => {
    if (typeof deg === 'number') {
        right(state,{deg:-deg});
    } else state.errors.push(errorMessages.left);
}

const methods = {
    forward: forward,
    backward: backward,
    goTo: goTo,
    right: right,
    left: left,
}
export default methods;
import RGBColor from './color_parser.js';
import errorMessages from './error_messages.js';

const setColor = (state,{r,g,b,a}) => {
    const {arrow} = state;
    const isAppropriateValue = (v) => {
        return typeof v === "number" && v >= 0 && v <= 255 && Number.isInteger(v)
    }
    if (typeof r === "string") {
        let [r1,g1,b1] = new RGBColor(r).toRGB_array();
        if (r !== "black" && r1 === 0 && g1 === 0 && b1 === 0) {
            state.errors.push(errorMessages["setColor"]);
        };
        arrow.color.r = r1;
        arrow.color.g = g1;
        arrow.color.b = b1;
        arrow.color.a = 1;
    } else if (
        isAppropriateValue(r) &&
        isAppropriateValue(g) &&
        isAppropriateValue(b)) {
        arrow.color.r = r;
        arrow.color.g = g;
        arrow.color.b = b;
        arrow.color.a = a ? a : 1; 
    } else {
        state.errors.push(errorMessages["setColor"]);
    }
};
const setWidth = (state,{width}) => {
    const {arrow} = state;
    if (typeof width === "number" && Number.isInteger(width)) {
        arrow.width = width;
    } else {
        state.errors.push(errorMessages["setWidth"]);
    }
}

const showCoords = ({arrow}) => {arrow.coords = true};

const reset = ({arrow}) => {
    arrow.position =  {x: 0, y: 0};
    arrow.angle = 0;
    arrow.width = 1;
    arrow.color = {r: 0, g: 100, b: 0, a:1};
}

const hide = ({arrow}) => {
    arrow.visible = false;
}

const methods = {
    setColor: setColor,
    setWidth: setWidth,
    reset: reset,
    hide: hide,
    showCoords: showCoords,
};

export default methods;
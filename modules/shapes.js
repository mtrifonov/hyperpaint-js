import errorMessages from './error_messages.js';

const drawCircle = (state,{radius}) => {
    if (typeof radius === 'number') {
        let {arrow: {position: {x,y}, width, color: {r,g,b,a}},vctx} = state;
        vctx.beginPath();
        vctx.strokeStyle = `rgba(${r},${g},${b},${a})`;
        vctx.lineWidth = width;
        vctx.arc(x, y, radius, 0, 2 * Math.PI);
        vctx.stroke();
    } else state.errors.push(errorMessages.drawCircle);

};

const drawEmoji = (s, {code,radius}) => {
    radius = radius ? radius : 15;
    let {resources, arrow: {position: {x,y}, width,angle, color: {r,g,b,a}},vctx} = s;
    let im = resources[code];
    vctx.save();
    vctx.translate(x, y);
    vctx.transform(1,0,0,-1,0,0);
    vctx.rotate(angle);
    vctx.translate(-x, -y);
    vctx.drawImage(im,x-radius, y-radius, radius*2, radius*2);
    vctx.restore();
}


const clear = ({vctx},args) => {
    vctx.clearRect(-350,-350,700,700);
}


const methods = {
    drawCircle: drawCircle,
    drawEmoji: drawEmoji,
    clear: clear,
}


export default methods;
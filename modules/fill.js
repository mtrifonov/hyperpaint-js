
// Naive flood fill algorithm implementation,
// copied almost exactly from pseudocode example 
// in wikipedia
const fill = (state) => {
    let {
        virtualCanvas: { width, height },
        vctx,
        arrow: { position: target, color: fillColor }
    } = state;


    let imData = vctx.getImageData(0, 0, width, height);
    const matrix = new Uint32Array(imData.data.buffer);
    const visitedMatrix = new Uint8Array(700 * 700);
    target = { ...target };
    target.x = Math.round(target.x);
    target.y = Math.round(target.y);
    fillColor = { ...fillColor };
    fillColor.a = Math.round(fillColor.a * 255);
    const coordToIndex = (x, y) => {
        x = x + 350;
        y = -y + 350;
        return (y * width + x);
    }
    const getPixel = ({ x, y }) => {
        let i = coordToIndex(x, y);
        return matrix[i];
    };
    const setPixel = ({ x, y }, { r, g, b, a }) => {
        let i = coordToIndex(x, y);
        matrix[i] = (256 ** 3) * a + (256 ** 2) * b + (256) * g + r;
    };
    const isVisited = ({ x, y }) => {
        let i = coordToIndex(x, y);
        return visitedMatrix[i] === 1;
    };
    const setVisited = ({ x, y }) => {
        let i = coordToIndex(x, y);
        return visitedMatrix[i] = 1;
    };
    const outOfBounds = ({ x, y }) => x > width / 2 || x < -width / 2 || y > height / 2 || y < - height / 2;
    const targetColor = getPixel(target);
    const numToColArr = (num) => {
        let a = Math.floor(num / (256 ** 3));
        num -= a * (256 ** 3);
        let b = Math.floor(num / (256 ** 2));
        num -= b * (256 ** 2);
        let g = Math.floor(num / (256));
        num -= g * 256;
        let r = num;
        return [r, g, b, a];

    };
    const distance = (col1, col2) => {
        if (col1 === col2) return 0;
        let [r1, g1, b1, a1] = numToColArr(col1);
        let [r2, g2, b2, a2] = numToColArr(col2);
        return Math.sqrt((r1 - r2) ** 2 + (g1 - g2) ** 2 + (b1 - b2) ** 2 + (a1 - a2) ** 2);
    };
    const inside = (node) => distance(getPixel(node), targetColor) < 30 && !outOfBounds(node) && !isVisited(node);
    const Q = [];
    Q.push(target);
    let count = 0;
    const scan = (lx, rx, y, Q) => {
        let added = false;
        for (let i = lx; i <= rx; i++) {
            if (!inside({ x: i, y: y })) {
                added = false;
            } else if (!added) {
                Q.push({ x: i, y: y });
                added = true;
            }
        }
    }
    while (Q.length > 0) {
        let node = Q.shift();
        count++;
        let lx = node.x;
        while (inside({ x: lx - 1, y: node.y })) {
            setPixel({ x: lx - 1, y: node.y }, fillColor);
            setVisited({ x: lx - 1, y: node.y });
            lx -= 1;
        }
        while (inside({ x: node.x, y: node.y })) {
            setPixel({ x: node.x, y: node.y }, fillColor);
            setVisited({ x: node.x, y: node.y });
            node.x += 1;
        }
        scan(lx, node.x - 1, node.y + 1, Q);
        scan(lx, node.x - 1, node.y - 1, Q);
    }

    let newIm = new ImageData(new Uint8ClampedArray(matrix.buffer), imData.width);
    vctx.beginPath();
    vctx.rect(-350, -350, 700, 700);
    vctx.fill();
    vctx.putImageData(newIm, 0, 0);


};


export default fill;

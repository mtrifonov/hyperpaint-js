import errorMessages from './error_messages.js';

const lineForward = (state,{dist}) => {
    let {arrow: {angle, position: {x,y}}} = state;
    if (typeof dist === 'number') {
        let y_delta = Math.cos(angle) * dist;
        let x_delta = Math.sin(angle) * dist;
        x = x + x_delta;
        y = y + y_delta;
        lineTo(state,{x,y});
    } else state.errors.push(errorMessages.lineForward);

}

const lineTo = (state,{x,y}) => {
    let {arrow,vctx} = state;
    let {color: {r,g,b,a},width} = arrow;
    if (typeof x === 'number' && typeof y === 'number') {
        vctx.beginPath();
        vctx.moveTo(arrow.position.x,arrow.position.y);
        vctx.lineTo(x,y);
        vctx.strokeStyle = `rgba(${r},${g},${b},${a})`;
        vctx.lineWidth = width;
        vctx.stroke();
    
        arrow.position.x = x;
        arrow.position.y = y;
    } else state.errors.push(errorMessages.lineTo);

}

const methods = {
    lineForward: lineForward,
    lineTo: lineTo,
}
export default methods;
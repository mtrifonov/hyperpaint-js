import lineMethods from './lines.js';
import movementMethods from './movement.js';
import shapeMethods from './shapes.js';
import propertyMethods from './properties.js';
import fill from './fill.js';
import drawCoords from './coords.js';


/**
 * have to deal with this later
 */
import template_ from './template.js';


const methods = {
    ...lineMethods,
    ...movementMethods,
    ...shapeMethods,
    ...propertyMethods,
    fill: fill,
}


const render = (state,actionQueue) => {
    return new Promise((resolve, reject) => {
        detectPathSequences(actionQueue);
        loadResources(state).then(_ =>{
            for (let i = 0; i < actionQueue.length; i++) {
                let action = actionQueue[i];
                methods[action.type](state,action.args);
            }
            renderCTX(state);
            displayErrors(state);
            resolve(state);
        }, rejection => {
            state.errors.push(rejection);
            displayErrors(state);
            reject(state);
        });
    })
}

const displayErrors = (state) => {
    if (state.asyncMode) {
        for (let i = 0; i < state.errors.length; i++) {
            console.error(state.errors[i]);
        }
    }
}

const loadResources = async({resourcesToLoad,resources}) => {
    let arr = [];
    for (let i = 0; i < resourcesToLoad.length; i++) {
        let code = resourcesToLoad[i];
        arr.push(new Promise((resolve, reject) => {
            let im = document.createElement('img');
            im.crossOrigin = "Anonymous";
            im.src = `https://storage.googleapis.com/resources-programming-level-1/openmoji-svg-color/${code}.svg`;
            resources[code] = im;
            im.onload = () => resolve();
            im.onerror = () => reject(`Image loading error. "${code}" is not a valid emoji code.`);
        }));
    }
    await Promise.all(arr);
}


const detectPathSequences = (actionQueue) => {
    const isOneOf = (item,a) => {
        for (let i = 0; i < a.length; i++) {
            if (item === a[i]) return true;
        } 
        return false;
    }
    let pathToggle = false;
    let index = 0;
    while (index < actionQueue.length) {
        //console.log(index,[...actionQueue]);
        const action = actionQueue[index];
        //console.log(action);
        if (pathToggle) {
            if (action.type === "penDown") {
                actionQueue.splice(index,1);
                index--;
            } else if (action.type === "penUp") {
                pathToggle = false;
            } else if (!isOneOf(action.type,["goTo","forward","left","right","backward","setWidth","setColor"])) {
                pathToggle = false;
                //console.log(index);
                actionQueue.splice(index,0,{type:"penUp",args:null});
                index++;
            }
        } else {
            if (action.type === "penDown") {
                pathToggle = true;
            } else if (action.type === "penUp") {
                actionQueue.splice(index,1);
                index--;
            } 
        }
        index++;
    }
    //console.log(pathToggle);
    if (pathToggle) {
        actionQueue.push({type:"penUp",args:null});
    }
}

const renderCTX = ({ctx,virtualCanvas,canvas,arrow,coordLine}) => {
    ctx.clearRect(-canvas.width / 2, -canvas.height / 2, canvas.width, canvas.height);
    if (arrow.coords) drawCoords(ctx);
    ctx.save();
    ctx.setTransform(1,0,0,1,0,0);
    ctx.drawImage(virtualCanvas,0,0, canvas.width, canvas.height);
    ctx.restore();
    if (arrow.visible) drawArrow(arrow,ctx);
}

const drawArrow = (arrow,ctx) => {
    var x = arrow.position.x;
    var y = arrow.position.y;
    //console.log(x,y);
    var w = 10;
    var h = 15;
    ctx.save();
    ctx.translate(x, y);
    ctx.rotate(-arrow.angle);
    ctx.translate(-x, -y);
    ctx.beginPath();
    ctx.moveTo(x - w / 2, y);
    ctx.lineTo(x + w / 2, y);
    ctx.lineTo(x, y + h);
    ctx.closePath();
    ctx.fillStyle = `rgba(${arrow.color.r},${arrow.color.g},${arrow.color.b},${arrow.color.a})`;
    ctx.fill();
    ctx.restore();
}

export { render };
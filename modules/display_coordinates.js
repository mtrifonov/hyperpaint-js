

const displayCoordinates = (state) => {
    let {canvas,coordLine} = state;
    canvas.addEventListener('pointermove',({offsetX: x, offsetY: y})=> {
        x = Math.round(x - 350);
        y = Math.round(-y + 350);
        let xc = document.getElementById('x_val');
        let yc = document.getElementById('y_val');
        xc.innerHTML = x;
        yc.innerHTML = y;
    })
    canvas.addEventListener('pointerout',(e)=> {
        let xc = document.getElementById('x_val');
        let yc = document.getElementById('y_val');
        xc.innerHTML = "-";
        yc.innerHTML = "-";
    })
};
export default displayCoordinates;
import { render } from './modules/render.js';
import displayCoordinates from './modules/display_coordinates.js';

const state = {
    init: function() {
        this.canvas = document.getElementById('paintcanvas');
        this.virtualCanvas = document.createElement('canvas');
        this.renderScheduled = false,
        this.asyncMode = true,
        this.errors = [];
        this.arrow = {
            position: {x: 0, y: 0},
            angle: 0,
            width: 1,
            penDown: false,
            visible: true,
            color: {r: 0, g: 100, b: 0, a:1},
            coords: false,
        };
        this.resourcesToLoad = [],
        this.resources = {};      
        this.virtualCanvas.width = this.canvas.width;
        this.virtualCanvas.height = this.canvas.height;
        this.ctx = this.canvas.getContext('2d');
        this.vctx = this.virtualCanvas.getContext('2d');
        this.vctx.setTransform(1, 0, 0, -1,350,350);
        this.ctx.setTransform(1, 0, 0, -1,350,350);
    },
};
state.init();

const actionQueue = {
    add: function(action) {
        if (!state.renderScheduled) {
            state.renderScheduled = true;
            if (state.asyncMode) {
                setTimeout(() => {
                    this.render();
                },0);
            };
        };
        if (action.resource) {
            state.resourcesToLoad.push(action.resource);
        };
        Object.freeze(action);
        this.data.push(action);
    }, 
    render: function() {
        return new Promise((resolve,reject) => {
            render(state,[...actionQueue.data]).then(r => {
                state.renderScheduled = false;
                actionQueue.data = [];
                resolve(r);
            }).catch(e => reject(e));
        })
    },
    init: function() {
        this.data =  [];
        displayCoordinates(state);
    },
};
actionQueue.init();

/**
 * Movements
 */
window.goTo = (x,y) => actionQueue.add({type: "goTo", args: {x: x, y: y}});
window.forward = (dist) => actionQueue.add({type: "forward", args: {dist: dist}});
window.backward = (dist) => actionQueue.add({type: "backward", args: {dist: dist}});
window.left = (deg) => actionQueue.add({type: "left", args: {deg:deg}});
window.right = (deg) => actionQueue.add({type: "right", args: {deg: deg}});
/**
 * Shapes
 */
window.drawCircle = (radius) => actionQueue.add({type: "drawCircle", args: {radius: radius}});;
window.drawEmoji = (code,radius) => actionQueue.add({type: "drawEmoji",args:{code:code,radius:radius},resource:code});
window.clear = () => actionQueue.add({type: "clear",args:null});
/**
 * Lines
 */
window.lineTo = (x,y) => actionQueue.add({type: "lineTo", args: {x: x, y: y}});
window.lineForward = (dist) => actionQueue.add({type: "lineForward", args: {dist: dist}});
/**
 * Properties
 */
window.setWidth = (width) => actionQueue.add({type: "setWidth", args: {width:width}});
window.setColor = (r,g,b,a) => actionQueue.add({type: "setColor", args: {r:r,g:g,b:b,a}});
window.reset = () => actionQueue.add({type: "reset",args: null});
window.hide = () => actionQueue.add({type: "hide",args:null});
window.showCoords = () => actionQueue.add({type: "showCoords",args:null});
/**
 * Other
 */
window.fill = () => actionQueue.add({type: "fill", args: null});
window.template = (id) => actionQueue.add({type: "template",args:{id:id}});


/**
 * For development and testing purposes only.
 */
window.hardReset = () => {state.init();actionQueue.init()};
window.render = actionQueue.render;
window.getState = () => state;
window.disableAsyncMode = () => state.asyncMode = false;






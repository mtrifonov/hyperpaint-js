import errorMessages from '../modules/error_messages.js';



/**
 *
 * setColor correct use
 */
const t1 = async () => {
    hardReset();
    disableAsyncMode();

    const {r: r1,g: g1,b: b1,a: a1} = getState().arrow.color;
    console.assert(r1 === 0 && g1 === 100 && b1 === 0 && a1 === 1)
    setColor("MidnightBlue");
    const {r: r2,g: g2,b: b2,a: a2} = await render().then(s => s.arrow.color);
    console.assert(r2 === 25 && g2 === 25 && b2 === 112 && a2 === 1)
    return;
};

/**
 *
 * setColor incorrect use
 */
 const t2 = async () => {
    hardReset();
    disableAsyncMode();
    setColor("Hello");
    const error = await render().then(s => s.errors[0]);
    console.assert(error === errorMessages["setColor"]);

    return;
};

/**
 *
 * setWidth incorrect use
 */
 const t3 = async () => {
    hardReset();
    disableAsyncMode();
    setWidth("Hello");
    const error = await render().then(s => s.errors[0]);
    console.assert(error === errorMessages["setWidth"]);
    return;
};






const runTests = async () => {
    await t1();
    await t2();
    await t3();
}

runTests();
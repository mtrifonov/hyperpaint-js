import errorMessages from '../modules/error_messages.js';
/**
 * 
 * lineForward correct use
 */
const t1 = async () => {
    disableAsyncMode();
    let y1 = await render().then(s => s.arrow.position.y);
    lineForward(100);
    let y2 = await render().then(s => s.arrow.position.y);
    console.assert(y1 === 0);
    console.assert(y2 === 100);
    hardReset();
    return;
};
/**
 * 
 * lineForward incorrect use
 */
const t2 = async () => {
    disableAsyncMode();
    lineForward("hello")
    let error = await render().then(s => s.errors[0]);
    console.assert(error === errorMessages["lineForward"]);
    hardReset();
    return;
};
/**
 * 
 * lineTo correct use
 */
 const t3 = async () => {
    disableAsyncMode();
    lineTo(100,200);
    let {x,y} = await render().then(s => s.arrow.position);
    console.assert(x === 100);
    console.assert(y === 200);
    hardReset();
    return;
};
/**
 * 
 * lineTo incorrect use
 */
const t4 = async () => {
    disableAsyncMode();
    lineTo("90");
    let error = await render().then(s => s.errors[0]);
    console.assert(error = errorMessages["lineTo"]);
    hardReset();
    return;
};




const runTests = async () => {
    await t1();
    await t2();
    await t3();
    await t4();
}

runTests();
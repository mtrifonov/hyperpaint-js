import errorMessages from '../modules/error_messages.js';
/**
 * 
 * forward correct use
 */
const t1 = async () => {
    disableAsyncMode();
    let y1 = await render().then(s => s.arrow.position.y);
    forward(100);
    let y2 = await render().then(s => s.arrow.position.y);
    console.assert(y1 === 0);
    console.assert(y2 === 100);
    hardReset();
    return;
};
/**
 * 
 * forward incorrect use
 */
const t2 = async () => {
    disableAsyncMode();
    forward("hello")
    let error = await render().then(s => s.errors[0]);
    console.assert(error === errorMessages["forward"]);
    hardReset();
    return;
};
/**
 * 
 * backward correct use
 */
const t3 = async () => {
    disableAsyncMode();
    let y1 = await render().then(s => s.arrow.position.y);
    backward(100);
    let y2 = await render().then(s => s.arrow.position.y);
    console.assert(y1 === 0);
    console.assert(y2 === -100);
    hardReset();
    return;
};
/**
 * 
 * backward incorrect use
 */
const t4 = async () => {
    disableAsyncMode();
    backward("hello")
    let error = await render().then(s => s.errors[0]);
    console.assert(error === errorMessages["backward"]);
    hardReset();
    return;
};
/**
 * 
 * right correct use
 */
const t5 = async () => {
    disableAsyncMode();
    right(90);
    let angle = await render().then(s => s.arrow.angle);
    console.assert(angle === Math.PI * 2 * 1/4);
    hardReset();
    return;
};
/**
 * 
 * right incorrect use
 */
const t6 = async () => {
    disableAsyncMode();
    right("90");
    let error = await render().then(s => s.errors[0]);
    console.assert(error = errorMessages["right"]);
    hardReset();
    return;
};
/**
 * 
 * left correct use
 */
 const t7 = async () => {
    disableAsyncMode();
    left(90);
    let angle = await render().then(s => s.arrow.angle);
    console.assert(angle === -Math.PI * 2 * 1/4);
    hardReset();
    return;
};
/**
 * 
 * left incorrect use
 */
const t8 = async () => {
    disableAsyncMode();
    left("90");
    let error = await render().then(s => s.errors[0]);
    console.assert(error = errorMessages["left"]);
    hardReset();
    return;
};
/**
 * 
 * goto correct use
 */
 const t9 = async () => {
    disableAsyncMode();
    goTo(100,200);
    let {x,y} = await render().then(s => s.arrow.position);
    console.assert(x === 100);
    console.assert(y === 200);
    hardReset();
    return;
};
/**
 * 
 * goto incorrect use
 */
const t10 = async () => {
    disableAsyncMode();
    goTo("90");
    let error = await render().then(s => s.errors[0]);
    console.assert(error = errorMessages["goTo"]);
    hardReset();
    return;
};




const runTests = async () => {
    await t1();
    await t2();
    await t3();
    await t4();
    await t5();
    await t6();
    await t7();
    await t8();
    await t9();
    await t10();
}

runTests();
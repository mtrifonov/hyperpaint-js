import errorMessages from '../modules/error_messages.js';

const compareCanvas = (c1,c2) => {
    const diff = (x,y) => Math.abs(x-y);
    let totalDiff = 0;
    for (let i = 0; i < c1.length; i++) {
        totalDiff += diff(c1[i],c2[i]);
    };
    const avgDiff = totalDiff / c1.length;
    const threshold = 0.001;
    if (avgDiff < threshold) return true;
    else return false;
}

/**
 *
 * drawCircle correct use
 */
const t1 = async () => {
    const canvas = document.createElement('canvas');
    canvas.width = 700;
    canvas.height = 700;
    const ctx = canvas.getContext("2d");
    ctx.beginPath();
    ctx.ellipse(350,350,100,100,0,0,Math.PI*2);
    ctx.strokeStyle = 'rgba(0,100,0)'
    ctx.stroke();
    const im1 = ctx.getImageData(0,0,700,700).data;

    hardReset();
    disableAsyncMode();
    drawCircle(100);
    hide();
    const im2 = await render().then(s => s.ctx.getImageData(0,0,700,700).data)
    console.assert(compareCanvas(im1,im2));
    return;
};
/**
 *
 * drawCircle incorrect use
 */
const t2 = async () => {
    hardReset();
    disableAsyncMode();
    drawCircle("hello");
    let error = await render().then(s => s.errors[0]);
    console.assert(error === errorMessages["drawCircle"]);
    return;
};

/**
 *
 * drawEmoji correct use
 */
 const t3 = async () => {
    const canvas = document.createElement('canvas');
    canvas.width = 700;
    canvas.height = 700;
    const ctx = canvas.getContext("2d");
    const emoji = document.createElement('img');
    emoji.src = "turtle2/tests/E0B4.svg";
    await emoji.decode();
    ctx.drawImage(emoji,250,250,200,200);
    const im1 = ctx.getImageData(0,0,700,700).data;
    //getState().ctx.putImageData(im1,0,0);

    hardReset();
    disableAsyncMode();
    drawEmoji("E0B4",100);
    hide();
    const im2 = await render().then(s => s.ctx.getImageData(0,0,700,700).data)
    console.assert(compareCanvas(im1,im2));
    return;
};
/**
 *
 * drawEmoji incorrect use
 */
 const t4 = async () => {
    hardReset();
    disableAsyncMode();
    drawEmoji("hello");
    const error = await render()
    .then(_ => _)
    .catch(s => s.errors[0])
    console.assert(error === `Image loading error. "hello" is not a valid emoji code.`);
    return;
};






const runTests = async () => {
    await t1();
    await t2();
    await t3();
    await t4();
}

runTests();